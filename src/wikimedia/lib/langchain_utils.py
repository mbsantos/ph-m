import operator
# import dotenv
# dotenv.load_dotenv() 
from langchain_core.agents import AgentAction, AgentFinish
from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder
from langchain_core.messages import (
    BaseMessage,
    FunctionMessage,
    HumanMessage,
)
from langchain.tools.render import format_tool_to_openai_function
from typing import Annotated, Sequence, TypedDict, Union
# from typing_extensions import TypedDict

tools = []

## Create Agents
def create_agent(llm, tools, system_message: str, action_description: str):
    """Create an agent with a given LLM, tools, system message, and action description."""
    functions = [format_tool_to_openai_function(t) for t in tools] if tools else []

    prompt = ChatPromptTemplate.from_messages(
        [
            (
                "system",
                """
                You are supporting a Product Manager to write weekly reports based on SDLC and product updates.

                Additional Instructions: {system_message}
                """
            ),
            MessagesPlaceholder(variable_name="input"),
        ]
    )
    prompt = prompt.partial(system_message=system_message)

    if functions:
        prompt = prompt.partial(tool_names=", ".join([tool.name for tool in tools]))
        return prompt | llm.bind_functions(functions)
    else:
        # If there are no functions, set tool_names to 'None'.
        prompt = prompt.partial(tool_names="None")
        return prompt | llm

class AgentState(TypedDict):
    # The input string
    input: Annotated[Sequence[BaseMessage], operator.add]
    # The list of previous messages in the conversation
    chat_history: list[BaseMessage]
    # The outcome of a given call to the agent
    # Needs `None` as a valid type, since this is what this will start as
    agent_outcome: Union[AgentAction, AgentFinish, None]
    # List of actions and corresponding observations
    # Here we annotate this with `operator.add` to indicate that operations to
    # this state should be ADDED to the existing values (not overwrite it)
    intermediate_steps: Annotated[list[tuple[AgentAction, str]], operator.add]
    # Name of the Sender Agent
    sender: str

# Define logic that will be used to determine which conditional edge to go down
def should_continue(data):
    # If the agent outcome is an AgentFinish, then we return `exit` string
    # This will be used when setting up the graph to define the flow
    if isinstance(data["agent_outcome"], AgentFinish):
        return "end"
    # Otherwise, an AgentAction is returned
    # Here we return `continue` string
    # This will be used when setting up the graph to define the flow
    else:
        return "continue"