api_platform_engineers = [
    {
        'email': 'aschulz@wikimedia.org',
        'gerrit_id': 16,
        'phab_name': 'aaron',
        'phid': ''
    },
    {
        'email': 'pnjira@wikimedia.org',
        'gerrit_id': 10261,
        'phab_name': 'Atieno',
        'phid': ''
    },
    {
        'email': 'bpirkle@wikimedia.org',
        'gerrit_id': 6178,
        'phab_name': 'BPirkle',
        'phid': ''
    },
    {
        'email': 'dkinzler@wikimedia.org',
        'gerrit_id': 128,
        'phab_name': 'daniel',
        'phid': ''
    },
    {
        'email': 'fgoodwin@wikimedia.org',
        'gerrit_id': 9375,
        'phab_name': 'FGoodwin',
        'phid': ''
    },
    {
        'email': 'wquarshie@wikimedia.org',
        'gerrit_id': 9332,
        'phab_name': ''
    }
]
mw_platform_engineers = [
    {
        'email': 'krinkle@fastmail.com',
        'gerrit_id': 34,
        'phab_name': 'Krinkle',
        'phid': ''
    },
    {
        'email': 'hokwelum@wikimedia.org',
        'gerrit_id': 9922,
        'phab_name': 'Hokwelum',
        'phid': ''
    },
    {
        'email': 'dziewonski@fastmail.fm',
        'gerrit_id': 417,
        'phab_name': 'matmarex',
        'phid': ''
    },
    {
        'email': 'dalangi-ctr@wikimedia.org',
        'gerrit_id': 2842,
        'phab_name': 'DAlangi_WMF',
        'phid': ''
    },
    {
        'email': 'gtisza@wikimedia.org',
        'gerrit_id': 389,
        'phab_name': 'Tgr',
        'phid': ''
    },
    {
        'email': 'pmiazga@wikimedia.org',
        'gerrit_id': 3945,
        'phab_name': 'Pmiazga',
        'phid': ''
    },
    {
        'email': 'ariel@wikimedia.org',
        'gerrit_id': 6,
        'phab_name': 'ArielGlenn'
    }
]
ctt_engineers = [
    {
        'email': 'ihurbainpalatin@wikimedia.org',
        'gerrit_id': 9322,
        'phab_name': 'ihurbain',
        'phid': 'PHID-USER-ut3yptb7r45gfpwp33qn'
    },
    {
        'email': 'cananian@wikimedia.org',
        'gerrit_id': 664,
        'phab_name': 'cscott',
        'phid': 'PHID-USER-m2ezqyeb4uz67zq6bats'
    },
    {
        'email': 'sbailey@wikimedia.org',
        'gerrit_id': 5354,
        'phab_name': 'Sbailey',
        'phid': 'PHID-USER-rrxwbet6ejrhu2kxridy'
    },
    {
        'email': 'ssastry@wikimedia.org',
        'gerrit_id': 257,
        'phab_name': 'ssastry',
        'phid': 'PHID-USER-slccyo5rqasgpljxny7g'
    },
    {
        'email': 'jgiannelos@wikimedia.org',
        'gerrit_id': 8206,
        'phab_name': 'Jgiannelos',
        'phid': 'PHID-USER-gh6rbo243sqey47hasxq'
    },
    {
        'email': 'abreault@wikimedia.org',
        'gerrit_id': 910,
        'phab_name': 'Arlolra',
        'phid': 'PHID-USER-gxcjmjpejlhfnrquksm2'
    },
    {
        'email': 'msantos@wikimedia.org',
        'gerrit_id': '',
        'phid': 'PHID-USER-3yahbwrgfgw2shcqbrf2',
        'phab_name': 'MSantos'
    }
]

all_engineers = api_platform_engineers + mw_platform_engineers + ctt_engineers