import re
import requests
import numpy as np
import json

from lib.gerrit import get_patch_details, get_patch_ids_for_bug

def extract_transaction_event(text):
    regex_pattern = r"^(.+) moved this task from (.+) to (.+) on the (.+?) board\.$"
    match = re.match(regex_pattern, text)

    if match:
        groups = match.groups()
        event_object = {
            "user": groups[0],
            "from_column": groups[1],
            "to_column": groups[2],
            "board_name": groups[3]
        }
        return event_object
    else:
        return None

def calculate_effort(patch_size, patch_sets, reviewer_comments):
    size_factor = 2  # Effort factor per size
    patch_set_factor = 0.5  # Effort factor per patch set
    comment_factor = 0.1  # Effort factor per reviewer comment
    
    effort = (patch_size * size_factor) + (patch_sets * patch_set_factor) + (reviewer_comments * comment_factor)
    return effort

def calculate_task_effort(bug_id, debug = False):
    patch_ids = get_patch_ids_for_bug(bug_id)
    # # Example patch IDs
    # patch_ids = ['575836']  # 905689
    total_effort = 0
    if patch_ids is None:
        print("No patch IDs found.")
    else:
        for patch_id in patch_ids:
            patch_details = get_patch_details(patch_id)
            if (debug):
                print("patch_id: ", patch_id)
                print("patch_details: ", patch_details)
            if patch_details is not None:
                patch_size, patch_sets, reviewer_comments = patch_details
                if (debug):
                    print(f"Patch size: {patch_size}")
                    print(f"Patch sets: {patch_sets}")
                    print(f"Reviewer comments: {reviewer_comments}")
                effort = calculate_effort(patch_size, patch_sets, reviewer_comments)
                total_effort += effort

    return total_effort

import seaborn as sns
import matplotlib.pyplot as plt

def plot_effort_spent_over_time(df):
    pivot_df = df.pivot(index='date', columns='task', values='spent_effort')
    # Set the size of the plot
    plt.figure(figsize=(15, 10))

    # Plot the heatmap
    sns.heatmap(df, cmap='Blues', annot=True, fmt='.1f')

    # Add labels and title
    plt.xlabel('Task')
    plt.ylabel('Date')
    plt.title('Effort Spent Over Time by Task')

    # Show the plot
    plt.show()



def task_times_facet_grid(df):
    # Create a FacetGrid, which will create one plot for each 'task'
    g = sns.FacetGrid(df, col="task", col_wrap=4, height=4, aspect=1.5)
    g = g.map(plt.plot, "date", "spent_effort")

    # Add titles and labels
    g.set_axis_labels("Date", "Spent Effort")
    g.set_titles("Task: {col_name}")
    plt.subplots_adjust(top=0.9)
    g.fig.suptitle('Spent Effort Over Time by Task')

    # Show the plots
    plt.show()