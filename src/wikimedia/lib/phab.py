import pandas as pd

def calculate_time_between_transactions(transaction, previous_transaction):
    """Calculate the spent effort based on the difference in time between two transactions"""
    start_time = pd.to_datetime(previous_transaction['dateCreated'], unit='s')
    end_time = pd.to_datetime(transaction['dateCreated'], unit='s')
    spent_effort = (end_time - start_time).total_seconds() / 3600  # convert to hours
    return spent_effort