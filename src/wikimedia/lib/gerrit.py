
import json
import re
import requests
import csv

GERRIT_BASE_URL = "https://gerrit.wikimedia.org/r"

def count_patch_sets(messages):
    patch_sets = 0
    for message in messages:
        if 'newPatchSet' in message.get('tag', ''):
            patch_sets += 1
    return patch_sets

# Function to retrieve patch details from Gerrit API
def get_patch_details(patch_id):
    url = f"{GERRIT_BASE_URL}/changes/{patch_id}/detail"
    headers = {
        "Content-Type": "application/json; charset=UTF-8"
    }
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        raw_response = response.content.decode('utf-8')
        # Remove the unwanted characters from the beginning of the response
        json_string = raw_response.lstrip(")]}'")
        patch_data = json.loads(json_string)
        # print(patch_data)
        patch_size = patch_data['insertions'] + patch_data['deletions']
        patch_sets = count_patch_sets(patch_data['messages'])
        reviewer_comments = patch_data['total_comment_count']
        return patch_size, patch_sets, reviewer_comments
    else:
        print(f"Failed to fetch details for patch {patch_id}")
        return None
    
def get_patch_simple_details(patch_id):
    query_changes_params = {
        "q": f"change:{patch_id}",
        "o": "CURRENT_REVISION"  # You can add more options as needed
    }
    changes = query_changes(query_changes_params)

    # TODO: replicate the other details function

def query_changes(params):
    url = f"{GERRIT_BASE_URL}/changes/"
    headers = {
        "Content-Type": "application/json; charset=UTF-8"
    }
    response = requests.get(url, params=params, headers=headers)
    if response.status_code == 200:
        raw_response = response.content.decode('utf-8')
        json_string = raw_response.lstrip(")]}'")
        changes = json.loads(json_string)
        return changes
    else:
        return None


def get_patch_ids_for_bug(bug_id):
    params = {
        "q": f"bug:{bug_id}",
        "o": "CURRENT_REVISION"  # You can add more options as needed
    }
    changes = query_changes(params)    
    if changes != None:
        patch_ids = [str(change['_number']) for change in changes]
        return patch_ids
    else:
        print(f"Failed to fetch patch IDs for bug {bug_id}")
        return None

# Function to get account id from Gerrit
def get_account_id(email):
    response = requests.get(f"{GERRIT_BASE_URL}/accounts/{email}")
    if response.status_code == 200:
        raw_response = response.content.decode('utf-8')
        json_string = raw_response.lstrip(")]}'")
        data = json.loads(json_string)
        return data['_account_id']

    else:
        return None
    
# Function to get email from Gerrit based on account ID
def get_email(account_id):
    response = requests.get(f"{GERRIT_BASE_URL}/accounts/{account_id}")
    if response.status_code == 200:
        raw_response = response.content.decode('utf-8')
        json_string = raw_response.lstrip(")]}'")
        data = json.loads(json_string)
        return data['email']
    else:
        return None

def fetch_change_details(change_id):
    """
    Fetch detailed information for a given change ID from Gerrit's REST API.
    """
    url = f"{GERRIT_BASE_URL}/changes/{change_id}/detail"
    response = requests.get(url)
    cleaned_response = response.text.lstrip(")]}'\n")
    data = json.loads(cleaned_response)

    # Extract commit message and find linked bugs
    commit_message = data["revisions"][data["current_revision"]]["commit"]["message"]
    linked_bugs = re.findall(r"Bug: T(\d+)", commit_message)

    # Compile and return the details
    return {
        "commit_message": commit_message,
        "linked_bugs": linked_bugs
    }

def fetch_commit_details(change_id):
    """
    Fetch commit details for the current revision of a given change ID from Gerrit's REST API.
    """
    # Endpoint for the current revision's commit details
    url = f"{GERRIT_BASE_URL}/changes/{change_id}/revisions/current/commit"
    response = requests.get(url)
    cleaned_response = response.text.lstrip(")]}'\n")
    commit_data = json.loads(cleaned_response)

    # Extract the commit message
    commit_message = commit_data.get("message", "")

    # Find linked bugs in the commit message, assuming they are formatted as "Bug: T12345"
    linked_bugs = re.findall(r"Bug: T(\d+)", commit_message)

    return {
        "commit_message": commit_message,
        "linked_bugs": linked_bugs
    }


def query_engineer_changes(start_date, engineer):
    print(start_date, engineer)
    params = {
        "q": f"after:\"{start_date}\" author:{engineer['email']}",
        "o": "CURRENT_REVISION"  # You can add more options as needed
    }

    changes = query_changes(params)
    detailed_changes = []

    for change in changes:
        change_id = change['_number']  # Assuming the change dictionary contains the '_number' key for the change ID.
        change_details = fetch_commit_details(change_id)

        # Merging the detailed information with the change
        change.update(change_details)
        detailed_changes.append(change)

    return detailed_changes




def get_changes_reviewed_by_mw_engineering(all_engineers, params, csv_file, suppress_reviewers=False):
    # Initialize an empty list to hold the clauses
    reviewers = []

    # Create a clause for each engineer
    if not suppress_reviewers:
        for engineer in all_engineers:
            clause = f"(label:Code-Review=+2 reviewer:{engineer['email']} OR label:Code-Review=+1 reviewer:{engineer['email']})"
    else:
        clause = ""
    reviewers.append(clause)

    params['q'] += f" {' OR '.join(reviewers)}"
    print('Query:', params['q'])
    all_changes = []

    with open(csv_file, 'w', newline='') as file:
        writer = csv.writer(file)
        # Extract column names from the first change
        column_names = None

        while True:
            changes = query_changes(params)
            print(changes)
            if not changes:
                break
            all_changes.extend(changes)
            print(f"Current start: {params['start']}, Total changes: {len(all_changes)}")
            if column_names is None:
                # Extract column names from the first change
                column_names = list(changes[0].keys())
                writer.writerow(column_names)

            for change in changes:
                row = [change.get(column, '') for column in column_names]
                writer.writerow(row)

            params['start'] += params['n']

    return all_changes